SELECT     SecTypeBaseCode + PrincipalCurrencyCode AS Type, SecurityID, SecTypeBaseCode, PrincipalCurrencyCode, ExchangeID, SecurityGUID, SymbolValidFromDate, 
                      SymbolValidThruDate, SymbolNameSpace, Symbol, SymbolTypeCode, CUSIP, Ticker, SEDOL, ISIN, OptionSymbol, ProprietarySymbol, FullName, 
                      UnderlyingSecurityID, InterestOrDividendRate, NextPaymentDate, convert(varchar,MaturityDate,10) as MaturityDate, MoodyRating, SPRating, FitchRating, PaymentFrequencyID, ShortAssetClassCode, 
                      LongAssetClassCode, RiskCountryCode, IssueCountryCode, StrategyID, StateCode, ValuationFactor, IsTradable, IsTradingCash, EPSAnnual, EPSLatest4Q, BookValue, 
                      Alpha, Beta, SharesOutstanding, SourceID, IsIncomplete, IsSystem, PriceThruCode, SourcePrefID, advapp.vindustrysector.SectorCode, advapp.vIndustryGroup.IndustryGroupCode , SecUserDef1ID, SecUserDef2ID, 
                      SecUserDef3ID, IsFixedIncome, CMOPaymentTypeCode, AccrualCalendarCode, BondInsurerID, BondRevenueSourceID, AmortizeToCode, EMDRuleCode, 
                      CouponDelayDays, CouponDelayRuleCode, CouponDelayHolidayRuleCode, InflationIndexID, FlowMethodCode, OriginalWeightedAvgMaturity, FirstCouponDate, 
                      LastCouponDate, IssueDate, IssuePrice, IssueSize, IsZeroCoupon, PayOnMonthEnd, TradeWithAI, RecordDate, EstMaturityDate, PoolNumber, DurationToMaturity, 
                      YTMOnMarket, AverageLife, ConversionFactor, InflationIndexBaseDate, IsVRS, PutScheduleMethodID, PutScheduleRuleID, PutScheduleFrequency, 
                      PutScheduleHolidayRuleCode, PutScheduleFirstDate, CouponPaymentMethodID, CouponPaymentDateRateID, CouponPaymentRuleID, CouponPaymentFrequency, 
                      CouponPaymentHolidayRuleCode, CouponResetMethodID, CouponResetDateRateID, CouponResetRuleID, CouponResetFrequency, CouponResetHolidayRuleCode, 
                      CouponResetFirstDate, CouponResetOffsetDays, CouponResetOffsetRate, CouponResetMultiplier, IsPerpetual
FROM         AdvApp.vSecurity left join advapp.vIndustryGroup on advapp.vsecurity.IndustryGroupID = advapp.vIndustryGroup.IndustryGroupID 
	left join advapp.vIndustrySector on advapp.vSecurity.SectorID = advapp.vIndustrySector.SectorID 
WHERE	SecTypeBaseCode <> 'br'

